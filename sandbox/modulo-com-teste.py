# -*- coding: utf-8 -*-

import unittest

# Nossa unidade
def IsOdd(n):
    return n % 2 == 1

# Nosso teste unitário
class IsOddTest(unittest.TestCase):

    def testOne(self):
        self.failUnless(IsOdd(1))

    def testTwo(self):
        self.failIf(IsOdd(2))

def main():
    unittest.main()


if __name__ == '__main__':
    main()
