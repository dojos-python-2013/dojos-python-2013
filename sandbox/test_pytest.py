# -*- coding: utf-8 -*-

def primos_ate(n):
    if n < 2:
        return []
    primos = [2]
    for i in range(3,n+1):
        for j in range(len(primos)):
            if i % primos[j] == 0:
                break
        else:
            primos.append(i)
    return primos

def fatora(n):
    fatores = []
    for i in primos_ate(n):
        if n % i == 0:
            fatores.append(i)
    return fatores

def test_primos_ate():
    assert primos_ate(0) == []
    assert primos_ate(1) == []
    assert primos_ate(2) == [2]
    assert primos_ate(5) == [2,3,5]
    assert primos_ate(12) == [2,3,5,7,11]

def test_fatores():
    assert fatora(3) == [3]
    assert fatora(6) == [2,3]
    assert fatora(12) == [2,3]
    assert fatora(60) == [2,3,5]
