# Sobre

Este subdiretório contém os arquivos usados nos Dojos Python 
do PoliGNU, ocorridos no primeiro semestre de 2013.

# Diretórios

- `dojo-YYYY-MM-DD`: programas produzidos no dia YYYY/MM/DD.
- `enunciados`: desafios para os dojos
- `sandbox`: utilidades, testes de framework, miscelânea
